import Image from "./myImge.png"

import boxStyle from "./BoxStyle.module.css"
import './App.css';

// icons

import instgram from "./icons/instagram.png"
import github from "./icons/github.png"
import linkedin from "./icons/linkedin.png"
import send from "./icons/send.png"
import stackOverflow from "./icons/stack-overflow.png"
import virus from "./icons/virus.png"

function App() {
  return (
    <div className={boxStyle.TheBox}>
      <div className={boxStyle.TheHeader}>
        <img className={boxStyle.Myimage} src={Image} alt='My Personal image'/>
        <div className={boxStyle.PersonalInfo}>
          <h1>Eddoughri Youssef</h1>
          <h2>Cloud Engineer</h2>
          <ul className={boxStyle.Social}>
            <a href="mailto:eddoughri.youssef@gmail.com"><li><img className={boxStyle.Icons} src={send} alt=" icon of send" /></li></a>
            <a href="https://www.linkedin.com/in/youssef-eddoughri-342b9117b/"><li><img className={boxStyle.Icons} src={linkedin} alt=" icon of linkedin" /></li></a>
            <a href="https://github.com/WhiteRose404"><li><img className={boxStyle.Icons} src={github} alt=" icon of github" /></li></a>
            <a href="https://stackoverflow.com/users/19416764/youssef-eddoughri"><li><img className={boxStyle.Icons} src={stackOverflow} alt=" icon of stackOverflow" /></li></a>
            <a href="https://www.instagram.com/_high_sparrow/"><li><img className={boxStyle.Icons} src={instgram} alt=" icon of instgram" /></li></a>
            <a href="#"><li><img className={boxStyle.Icons} src={virus} alt=" icon of virus" /></li></a>
          </ul>
        </div>
      </div>
      <div className={boxStyle.TheBody}>
        <p>Temporary website 😉.</p>
        <p>But Be aware the are traps out there !!</p>
      </div>
      <div className="TheFooter">
      <ul className="social">
            <a href="https://document-public.s3.eu-west-3.amazonaws.com/Youssef_Eddoughri_v2.0.pdf"><li>CV</li></a>
          </ul>
      </div>
    </div>
  );
}

export default App;
