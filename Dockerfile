FROM node

# copy the package json and install the depedency"q first
# to leverge the cache
RUN mkdir /app
WORKDIR /app
COPY ./app/package.json .
RUN npm install

# to Leverge the cache also
COPY /app/public ./public 

# to Leverge the cache also
COPY /app/src ./src 


# build the project
RUN npm run build


# install the server (should use nginx in the futur)
RUN npm install -g serve

# Expose the port
EXPOSE 3000

# serve the app
CMD [ "serve", "-s", "build" ]